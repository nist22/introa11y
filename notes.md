# Introduction

The course [Introduction to Web Accessibility](https://www.edx.org/course/web-accessibility-introduction) was created by the [W3C](https://www.w3.org/) to educated the public on accessiblity.

It is not specially for web developer or professional.

## Module 1: What is Web Accessiblity

### Trainers

- Shawn Lawton Henry from [W3C Web Accessibility Initiative](https://www.w3.org/WAI/)
- Anthony Vasquez from [Knowbility](https://knowbility.org/)

### Technology

Accessibility is about people, not technology or standard.

Some examples :

- text-to-speech app
- joystick instead of a mouse
- mouth stick
- screen reader

The screen reader is too fast and need to be slowed down.
If the website isn't well structured, it's quite hard to figure it out and find our way through it.

There's also the importance for good alt texts, something meaningful for someone who will just heard it.

The screen reader can expand some items, like a list on the website, by telling what's in the list.

People who are deaf and blind can use a digital braille display to *read* with their fingers.

Some images, like logos and emojis, are quite hard to understand with a screen reader. 

There's not only blind people who use screen reader.

People can use screen reader to help them better understand the information.

Speech recognition could be use for people who can't use a keyboard.

Caption and transcripts are ueful for deaf people and people with difficulties to hear well.

Some people use the web only with the keyboard (**tab**,**enter**, **arrow key**, **escape**).

The way the text is wrapped or not will also facilitate the life of people with disabilities.

It's a good thing if there's not need to scroll **left** **right** to read the text.

The spacing of the text also help the reading.

In CSS :

`* {
line-height: 1.5 !important;
letter-spacing: 0.12em !important;
word-spacing: 0.16em !important;
}
p {
margin-bottom: 2em !important;
}`


### Accessibility is for everyone

The web is use on all kind of devices, not only computers.

Accessibility means web and technologies are designed and developed to be usable by people with disabilities.

Assumptions can be noxious because something could be well meant, but doesn't work for the user.

It's important to test the real utilization to be aware of what is working and what's not.

There's also other cases like :

- Temporary disabilities
- Signs language using a webcam
- low bandwith connection or old hardware using alt text
- small screens
- older people
- situational limitations like being outside

Video :

- Being clear
- Simple language

Web page :

- Being explicit (no *click here* for example)
- Good design

## Module 2: People and Digital Technology

Trainer Henny Swan from [The Paciello Group](https://www.tpgi.com/).

**Assistive technologies**: hardware or software that allows people with disabilities to interact in the digital world.

Example : screen reader

**Adaptive strategies**: techniques used by people with disabilities to adjust their environment to their needs.

Example : browser settings

### Keyboard accessibility

Allow to use the keyboard to navigate and interact with elements of the web page.

**Important**

- tooltips: often only accessible with the mouse.
- focus:  
	- make it visible.
	- follow the order of the elements on the page.
- skip links: with a lengthy navigation, allow to skip to main content.

[Keyboard Accessibility according to WebAim](https://webaim.org/techniques/keyboard/)

### Screen reader

Examples :

- [Jaws on Windows](https://www.freedomscientific.com/products/software/jaws/)
- [NVDA on Windows](https://www.nvaccess.org/)
- [VoiceOver on MacOS and iOS](https://www.apple.com/voiceover/info/guide/_1121.html)
- [TalkBack on Android](https://blog.google/products/android/all-new-talkback/)
- [Orca on Linux](https://help.gnome.org/users/orca/stable/)

#### Braille displays :

- [American Foundation for the Blind](https://www.afb.org/node/16207/refreshable-braille-displays)
- [boundless](https://www.boundlessat.com/Blindness/Braille-Displays)
- [eurobraille](https://www.eurobraille.com/product-category/product-catalogue/eurobraille-braille-displays/)

### Low Vision

- Font size
- Poor contrast
- Relying on color
- Point of regard : place where we are reading

#### Screen magnification

- [Zoomtext](https://www.zoomtext.com/)
- [On Windows](https://support.microsoft.com/en-us/topic/setting-up-and-using-magnifier-e1330ccd-8d5c-2b3c-d383-fd202808c71a)
- [On MacOS](https://support.apple.com/en-us/HT210978)
- [On Linux](https://ostechnix.com/how-to-magnify-screen-areas-on-linux-desktop/)
- [On iOS](https://support.apple.com/en-ca/guide/iphone/iphd6804774e/ios)
- [On Android](https://support.google.com/accessibility/android/answer/6006949?hl=en)

### Hearing disabilities

- Closed captions
- Subtitles
- Transcripts
- Sign language

### Cognitive

- Simple navigation and page layout.
- Text easy to understand (vocabulary, structure).
- Images offering an extra value and easy to understand.
- Autoplays and animations should allow to be turned off. 

[Cognitive Accessiblity 103](https://spacedoutandsmiling.com/presentations/cognitive-accessibility-103-csun-2017)


### Where people meet digital technology

- [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/)
- [Authoring Tool Accessibility Guidelines (ATAG)](https://www.w3.org/WAI/standards-guidelines/atag/)
- [User Agent Accessibility Guidelines (UAAG)](https://www.w3.org/WAI/standards-guidelines/uaag/)

## Module 3: Business Cases and Benefits

Strategies to implement accessibility from a business point of view.

- Return on Investment (ROI)
- Corporate Social Responsibility (CSR)

Also :

- brand reputation
- human centered design
- the legal issues associated with accessibility

Context :

- 15 to 20% of people requires accessibility
- accessibility helps :
	- mobile device users
	- older people
	- people with low literacy or not fluent in the language
	- users of older technologies
	- people with temporary disabilities

ROI :

- increase sales
- improve brand reputation
- drive innovation
- prevent litigation
- set as leader in industry 

[Laws & Policies](http://www.w3.org/WAI/policies/)


## Module 4: Principles, Standards, and Checks

Trainers :

- Makoto Ueki, [Infoaxia](https://infoaxia.co.jp/)
- Carie Fisher, [Deque Systems](https://www.deque.com/)

4 principles of Web Content Accessibility Guidelines (WCAG) :

1. **P**erceivable: information presented in different ways
2. **O**perable: functionality used in different modalities
3. **U**nderstandable: information and functionality are understandable
4. **R**obust: content can be interpreted by a variety of technologies


### W3C Web Accessibility Initiative (WAI)

- [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/)
- [Authoring Tool Accessibility Guidelines (ATAG)](https://www.w3.org/WAI/standards-guidelines/atag/)
- [User Agent Accessibility Guidelines (UAAG)](https://www.w3.org/WAI/standards-guidelines/uaag/)

### WCAG 2.0

- 2008
- 12 guidelines for the 4 POUR principles
- 61 sucess criteria divided in 3 levels : **A**, **AA**, **AAA**

### WCAG 2.1

- 2018
- Improvement for :
	- people with cognitive and learning disabilities
	- people with low vision
	- users of mobile devices
- Must first conform to WCAG 2.0
- Add 13 guidelines for the 4 POUR principles
- Add 78 sucess criteria divided in 3 levels : **A**, **AA**, **AAA**

### Basic accessibility checks

- [A First Review of Web Accessibility](https://www.w3.org/WAI/test-evaluate/preliminary/)
- [WAVE extension](https://wave.webaim.org/extension/)
- [Web developer toolbar](https://chrispederick.com/work/web-developer/)
- [Color contrast analyzer](https://www.tpgi.com/color-contrast-checker/)
- [**B**efore and **A**fter **D**emonstration](https://www.w3.org/WAI/demos/bad/)

### Perceivable

- text alternatives for images
	- if image is decorative (example: a border), it must be hidden to assistive technologies (example : screen reader).
	- text alternative must be:
		- meaningful
		- descriptive
		- no repetition
	- if image is actionable, must provide information about the function.

- Time-based media
	- audio
	- video
	- video with audio
	- alternatives methods:
		- captions
		- transcripts
		- audio description
		- sign language

- Adaptable content
	- content that can be presented in different ways
	- example: headings
		- Web Developer Toolbar->Outline->Headings
		*or*
		- Web Developer Toolbar->Information->View Document Outline

- Checking structure
	- Web Developer Toolbar->Images->Replace Images With Alt Attributes
	*and*
	- Web Developer Toolbar->CSS->Disable All Styles
	*and*
	- Web Developer Toolbar->Miscellaneous->Linearize page

- Distinguishable content

Make it easier for users to see and hear content including separating foreground from background.

- Checking contrast ratio

	- [Color Contrast Analyzer Tool](https://developer.paciellogroup.com/resources/contrastanalyser/)
	- [Color Contrast Checker](https://contrastchecker.com/)

- Checking text resize

	- Increase zoom in browser ([ctrl]+[+])
	- Increase text size in browser (Settings->Appearance->Font size)

### Operable

#### Keyboard accessible

Make all functionality available from a keyboard.

Check for:

- Tab to all controls and form elements
- Tab away: exit from this control/element
- Tab order: follow logical reading order
- Focus clearly visible
- All functionality by keyboard

#### Enough time

Provide users enough time to read and use content.

#### Avoid physical reactions and seizure

Do not design content in a way that is known to cause seizures or physical reactions.

Examples :

- Bliking
- Flashing
- Fast moving content
- Color patterns

#### Navigable content

Provide ways to help users navigate, find content, and determine where they are.

- Check page title : different for each page and meaningful. Most important information must appear first.

#### Input modalities

Make it easier for users to operate functionality through various inputs beyond keyboard.


### Understable

#### Readable content

Make text content readable and understandable.

#### Predictable content

Make Web pages appear and operate in predictable ways.

#### Input assistance

Help users avoid and correct mistakes.

Examples :

- Indicate required fields
- Validate form fields:
	- date format
	- number
	- specific format like phone number
- Use meaningful labels
- Provide clear instructions
- Show clear error report and way to solve it

### Robust

#### Compatible

Maximize compatibility with current and future user agents, including assistive technologies.

- [WAI-ARIA](https://www.w3.org/WAI/standards-guidelines/aria/)
- [How to Meet WCAG (Quick Reference)](https://www.w3.org/WAI/WCAG21/quickref/)

## Module 5: Getting Started with Accessibility in your Organization

[Planning and Managing Web Accessibility](https://www.w3.org/WAI/planning-and-managing/)

### Introduction to discover and plan

- Explore your current environment
- Set objectives and allocate resources
- Gather support and raise awareness
- Create your accessibility policy
- Create your accessibility statements
- Create a monitoring framework


[Tips for Getting Started](https://www.w3.org/WAI/tips/)

### Introduction to implement and maintain

- Accessibility is a shared responsibility
- Skills need to be built
- Products need to be created accessibility
- Changes need to be monitored
- Stakeholder engagement

### Training

[Developing Web Accessibility Presentations and Training](https://www.w3.org/WAI/teach-advocate/accessibility-training/)

## Conclusion

The material is a little bit outdated, but the examples are good to make people understand what is to live with somekind of disabilities and use the web.

To want to grab a large spectrum is also a weakness of the course : there's not enough details to really make a web developer efficient in accessibility or an organization aware of all the pros and cons of accessibility.